/**This function renders the form handler when the user clicks on add to contacts button */
function showFormHandler() {
    document.getElementById('buttonID').style.display = 'none';
    document.getElementById('contactInfoId').style.display = 'none';
    document.getElementById('formBodyId').style.display = 'block';
    document.getElementById('circularG').style.display = 'none';
}
/**This function renders te button when there is no contact found in the CRM */
function showButton(phoneNum) {
    document.getElementById('buttonID').style.display = 'flex';
    document.getElementById('contactInfoId').style.display = 'block';
    document.getElementById('phoneId').value = phoneNum;
}

/**This function is invoked when the agent submits the form and checks for the mandatory fields */
function formSubmitHandler(event) {
    event.preventDefault();

    /* disable button and enable loader in button */
    document.getElementById('formSubmitId').disabled = true;
    document.getElementById('formSubmitId').style.cursor = 'not-allowed';
    document.getElementById('submitTextId').style.display = 'none';
    document.getElementById('circularG').style.display = 'block';

    let fullName = document.getElementById('fname').value;
    let email = document.getElementById('email').value;
    let phoneNum = document.getElementById('phoneId').value;
    let postData = `{
                        "name":"${fullName}",
                        "handles":[
                                    {
                                        "handle":"${email}",
                                        "source":"email"
                                    },
                                    {
                                        "handle":"${phoneNum}",
                                        "source":"phone"
                                    }
                                ]   
                    }`;
    const newHeaders = { Accept: 'application/json', Authorization: 'Bearer <%=iparam.privateToken%>', 'Content-Type': 'application/json' };
    const newOptions = { headers: newHeaders, body: postData };
    const url = 'https://api2.frontapp.com/contacts';
    client.request
        .post(url, newOptions)
        .then(function(resData) {
            if (resData.status === 201) {
                /* hide the form */
                document.getElementById('formBodyId').style.display = 'none';

                /* show the success message and form data */
                document.getElementById('newContactMessageId').style.display = 'block';
                let updatedObj = JSON.parse(resData.response);
                if (updatedObj.name) {
                    document.getElementById('subjectFirstName').innerText = updatedObj.name;
                }
                for (handleDetail of updatedObj.handles) {
                    if (handleDetail.source === 'email') {
                        document.getElementById('subjectEmailId').innerText = handleDetail.handle;
                    }
                    if (handleDetail.source === 'phone') {
                        document.getElementById('subjectPhoneNo').innerText = handleDetail.handle;
                    }
                }
            }
        })
        .catch(function(e) {
            /* show e message and make email id field red when e found */
            if (e.status === 409) {
                document.getElementById('email').style.borderBottom = '1px solid red';
                document.getElementById('labelEmailId').style.color = 'red';
                document.getElementById('formErrorId').innerText = 'Email Id already exists';
                document.getElementById('formSubmitId').disabled = false;
                document.getElementById('circularG').style.display = 'none';
                document.getElementById('submitTextId').style.display = 'block';
                document.getElementById('formSubmitId').style.cursor = 'pointer';
            } else if (e.status === 400) {
                console.error('Error occured while creating contact', JSON.stringify(e));
                document.getElementById('formBodyId').style.display = 'none';
                let errorInCreatingContact = errorRenderForm(JSON.parse(e.response));
                $('#contactCreationErrorId').append(errorInCreatingContact);
            } else {
                console.error('Error occured while creating contact', JSON.stringify(e));
                let errorOccuredText = errorRender();
                $('#errorOccuredId').append(errorOccuredText);
            }
        });
}
