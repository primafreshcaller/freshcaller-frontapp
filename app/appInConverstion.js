const headers = { Accept: 'application/json', Authorization: 'Bearer <%=iparam.privateToken%>' };
const options = { headers: headers };
let client = '';
$(document).ready(function() {
    app.initialized()
        .then(function(_client) {
            client = _client;
            let inConversationDetailsPromise = inConversationDetailsFetch();
            inConversationDetailsPromise
                .then(function() {
                    console.log('Completed');
                })
                .catch(function(e) {
                    console.error('Error occured while fetching inConversation details', JSON.stringify(e));
                    let errorOccuredText = errorRender();
                    $('#errorOccuredId').append(errorOccuredText);
                });
        })
        .catch(function(e) {
            console.error('Error occured while initialising the document', JSON.stringify(e));
            let errorOccuredText = errorRender();
            $('#errorOccuredId').append(errorOccuredText);
        });
});

function inConversationDetailsFetch() {
    return new Promise(function(resolve, reject) {
        const customerDetailsPromise = getCustomerDetails();
        customerDetailsPromise
            .then(function(customerDetails) {
                let name = fetchCustomerName(customerDetails);
                let nameRenderInConversation = inConversationNameRender(name);
                $('#customerNameInConversation').append(nameRenderInConversation);
                let customerConversationsPromise = fetchCustomerConversations(customerDetails);
                customerConversationsPromise
                    .then(function(customerConversations) {
                        let latestConversationDetails = customerConversations._results[0];
                        let latestConversationSubject = latestConversationDetails.subject;
                        let latestConversationMessage = latestConversationDetails.last_message.blurb;
                        let latestConversationId = latestConversationDetails.id;
                        let messageInConversation = inConversationMessageRender(
                            latestConversationSubject,
                            latestConversationId,
                            latestConversationMessage
                        );
                        $('#inConversationMessageId').append(messageInConversation);
                    })
                    .catch(function(e) {
                        console.error('Error occured while fetching converstions', JSON.stringify(e));
                    });
                return resolve();
            })
            .catch(function(e) {
                if (e.status === 404) {
                    getPhoneNumber()
                        .then(function(phoneNum) {
                            showButton(phoneNum);
                            return resolve();
                        })
                        .catch(function(e) {
                            console.error('Error occured while fetching phoneNum', JSON.stringify(e));
                            return reject(e);
                        });
                } else {
                    console.error('No Contact Details', JSON.stringify(e));
                    return reject(e);
                }
            });
    });
}
