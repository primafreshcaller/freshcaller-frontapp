// API HELPERS
const urlMain = "https://app.frontapp.com";
/** This function returns the Promise  phoneNumber for an incoming call , using callDetails.caller.from
 * or
 * returns the phoneNumber of the dialled number for an outgoing call using callDetails.caller.to
 */
function getPhoneNumber() {
    return new Promise(function(resolve, reject) {
        client.data
            .get('currentCall')
            .then(function(callDetails) {
                if (callDetails.caller) {
                    if (callDetails.caller.from) {
                        let phoneNum = callDetails.caller.from;
                        return resolve(phoneNum);
                    } else {
                        let phoneNum = callDetails.caller.to;
                        return resolve(phoneNum);
                    }
                }
            })
            .catch(function(err) {
                console.error('Error occured while fetching phone number of the caller ', JSON.stringify(err));
                return reject(err);
            });
    });
}
/**This function returns the name of the customer from the property array */
function fetchCustomerName(customerDetails) {
    return customerDetails.name ? customerDetails.name : 'No Name Found' ;
}
/**This function returns the Promise with customer details if present and error if there is no csutomer details
 */
function getCustomerDetails() {
    return new Promise(function(resolve, reject) {
        let phoneNumPromise = getPhoneNumber();
        phoneNumPromise
            .then(function(phoneNum) {
                client.request
                    .get(`https://api2.frontapp.com/contacts/alt:phone:${phoneNum}`, options)
                    .then(function(customerDetailsRes) {
                        if (customerDetailsRes.response) {
                            let customerDetails = JSON.parse(customerDetailsRes.response);
                            return resolve(customerDetails);
                        }
                    })
                    .catch(function(e) {
                        console.error('Error occured while fetching Contact Details : ', JSON.stringify(e));
                        return reject(e);
                    });
            })
            .catch(function(e) {
                console.error('Error occured while fetching phoneNumber', JSON.stringify(e));
                return reject(e);
            });
    });
}

function fetchCustomerConversations(customerDetails) {
    return new Promise(function(resolve, reject) {
        const url = customerDetails._links.related.conversations;
        client.request
            .get(url, options)
            .then(function(customerConversationsRes) {
                if (customerConversationsRes.response) {
                    let customerConversations = JSON.parse(customerConversationsRes.response);
                    if (customerConversations._results.length) {
                        return resolve(customerConversations);
                    } else {
                        let noConversationText = `
                        <p class="recentConversation">Recent Conversation:</p>
                        <p class="noConversation">No Conversations</p>
                        `;
                        $('#notificationConversationId').append(noConversationText);
                        $('#inConversationMessageId').append(noConversationText);
                        return reject();
                    }
                }
            })
            .catch(function(e) {
                console.error('Error occured while fetching conversations', JSON.stringify(e));
                return reject(e);
            });
    });
}
// HTML HELPERS
function notificationNameRender(name) {
    const url = `${urlMain}/contacts`;
    let nameRenderNotification = `
                                <div class = "headerName">
                                Contact name:
                                </div>
                                <div class = "fullName">
                                <a href="${url}" class="nameLinkClassNotification" target="_blank">
                                ${name}
                                </a>
                                </div>
                                `;
    return nameRenderNotification;
}

function notificationConversationRender(latestConversationId, latestConversationSubject) {
    let conversationNotification = `
            <p class="recentConversationNotification">Recent Conversation:</p>
            <div class="subjectTitleClassNotification">SUBJECT</div>
            <a href="${urlMain}/open/${latestConversationId}" target="_blank" class="linkToConversationNotification">
            ${latestConversationSubject}</a>
            `;
    return conversationNotification;
}

function inConversationNameRender(name) {
    const url = `${urlMain}/contacts`;
    let nameRenderInConversation = `
                                    <div class = "headerName">
                                    Contact name:
                                    </div>
                                    <div class = "fullName">
                                    <a href="${url}" class="nameLinkClass" target="_blank">
                                    ${name}
                                    </a>
                                    </div>
                                    `;
    return nameRenderInConversation;
}

function inConversationMessageRender(latestConversationSubject, latestConversationId, latestConversationMessage) {
    let messageInConversation = `
                            <p class="recentConversation">Recent Conversation:</p>
                            <div class="subjectTitleClassInConversation">SUBJECT</div>
                            <div class="subjectBodyClassInConversation">${latestConversationSubject}</div>
                            <div class="messageTitleClass">Message</div>
                            <div class="messageBodyClass">${latestConversationMessage}</div>
                            <a href="${urlMain}/open/${latestConversationId}" target="_blank" class="linkToConversation">
                            View Conversation</a>
                            `;
    return messageInConversation;
}
/**ERROR RENDER HELPERS */
function errorRender() {
    let text = 'Oops! Some error occurred. Please try after sometime or contact tech support';
    let errorOccuredText = `<div class ="errorMessageBody">${text}</div>`;
    return errorOccuredText;
}
function errorRenderForm(errorMessageDetails) {
    let errorOccuredText = `<div class = "errorHeader">Error Occured!</div>
                                 <div class ="errorMessageHeader"> Message </div>
                                 <div class ="errorMessageBody">${errorMessageDetails._error.message}</div>
                                 `;
    if (errorMessageDetails 
            && errorMessageDetails._error 
            && Array.isArray(errorMessageDetails._error.details) 
            && errorMessageDetails._error.details.length ) {
        errorOccuredText = ` <div class = "errorHeader">Error Occured!</div>
        <div class ="errorMessageHeader"> Message </div>
        <div class ="errorMessageBody">${errorMessageDetails._error.message} , ${errorMessageDetails._error.details[0]}</div>`;
    }
    return errorOccuredText;
}
