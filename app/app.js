const headers = { Accept: 'application/json', Authorization: 'Bearer <%=iparam.privateToken%>' };
const options = { headers: headers };
let client = '';
$(document).ready(function() {
    app.initialized()
        .then(function(_client) {
            client = _client;
            let appActivatedPromise = notificationAppActivated();
            appActivatedPromise
                .then(function() {
                    console.log('Completed');
                })
                .catch(function(e) {
                    console.error('Error occured while Activating', JSON.stringify(e));
                    let errorOccuredText = errorRender();
                    $('#errorOccuredId').append(errorOccuredText);
                });
        })
        .catch(function(e) {
            console.error('Error occured while initialising the document', JSON.stringify(e));
            let errorOccuredText = errorRender();
            $('#errorOccuredId').append(errorOccuredText);
        });
});

function notificationAppActivated() {
    return new Promise(function(resolve, reject) {
        client.events.on('app.activated', function() {
            let notificationDetailsPromise = notficationDetailsFetch();
            notificationDetailsPromise
                .then(function() {
                    return resolve();
                })
                .catch(function(e) {
                    console.error('Error occured while fetching notification details', JSON.stringify(e));
                    return reject(e);
                });
        });
    });
}

function notficationDetailsFetch() {
    return new Promise(function(resolve, reject) {
        /**Fetch Customer details From FrontAPP*/
        const customerDetailsPromise = getCustomerDetails();
        customerDetailsPromise
            .then(function(customerDetails) {
                /**If Contact is present in FrontAPP */
                let name = fetchCustomerName(customerDetails);
                let nameRenderNotification = notificationNameRender(name);
                $('#customerNameNotification').append(nameRenderNotification);
                let customerConversationPromise = fetchCustomerConversations(customerDetails);
                customerConversationPromise
                    .then(function(customerConversations) {
                        let latestConversationDetails = customerConversations._results[0];
                        let latestConversationSubject = latestConversationDetails.subject;
                        let latestConversationId = latestConversationDetails.id;
                        let conversationNotification = notificationConversationRender(latestConversationId, latestConversationSubject);
                        $('#notificationConversationId').append(conversationNotification);
                    })
                    .catch(function(e) {
                        console.error('Error occured while fetching converstions', JSON.stringify(e));
                    });
                return resolve();
            })
            .catch(function(e) {
                /**If Contact is not present in FrontAPP */
                if (e.status === 404) {
                    let noDataString = 'No Contact Found!';
                    $('#noInfoId').append(noDataString);
                    return resolve();
                } else {
                    console.error('Contact Details Promise did not resolve', JSON.stringify(e));
                    return reject(e);
                }
            });
    });
}
