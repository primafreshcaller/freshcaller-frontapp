## Freshcaller-FrontAPP Integration

## Installation guidelines

1. Front APP Private Token
   Steps:-
   a. log in to FrontApp with your registered email address.
   b. Go to settings
   c. Go to Plugins and API section
   d. Go to API section
   e. Create a new token
   f. Select Private Resources in scopes section
   g. Copy the generated API key.

### Folder structure explained

    .
    ├── README.md                  This file
    ├── app                        Contains the files that are required for the front end component of the app
    │   ├── app.js                 JS to render the dynamic portions of the app
    │   ├── icon.svg               Sidebar icon SVG file. Should have a resolution of 64x64px.
    │   ├── freshcaller_logo.png   The Freshcaller logo that is displayed in the app
    │   ├── style.css              Style sheet for the app
    │   ├── template.html          Contains the HTML required for the app’s UI
    ├── config                     Contains the installation parameters and OAuth configuration
    │   ├── iparams.json           Contains the parameters that will be collected during installation
    │   └── iparam_test_data.json  Contains sample Iparam values that will used during testing
    └── manifest.json              Contains app meta data and configuration information

# freshcaller-frontapp
